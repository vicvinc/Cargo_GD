<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">请选择要查询的季度</p>
		<p>若要查询三个季度以外的账单请至年度账单查询</p>
		<p>当前日期为<%=date()%></p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">查询今年的第</span>
					<select class="span2" name="season">
						<%
							curDate=date()
							season=Request.form("season")
							curDate=date()
							curMonth=month(curDate)
							curSeason=curMonth\4+1
							for i=1 to curSeason
								season_i=i
						%>
						<option><%=season_i%></option>
						<%next%>
					</select>
					<span class="add-on">季度</span>
					<button class="btn btn-mini btn-info" formaction="./querySBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->