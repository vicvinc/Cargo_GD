<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span9 well">
	<p class="text-success">请选择配件信息</p>
	<form method="post" action="./queryPartsCheck.asp">
	    <div class="row-fluid">
	      <div class="span3 offset2">
	      		<label>配件名</label>
				<input class="input-small" name="partsName" type="text" placeholder="配件名">
	      </div>
	      <div class="span3">
	      	<span>供应商</span>
				<select name="supplierName">
					<option></option>
	      		<%
		      		sql="select * from supplier"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("supplier_name")%>"><%=rs("supplier_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
	      </div>
	      <div class="span3">
		      	<span>配件价格</span>
					<select name="partsPrice">
						<option></option>
		      		<%
			      		sql="select * from parts"
			      		rs.open sql,conn,1,3
			      		do while not rs.eof 
					%>
						<option value="<%=rs("price")%>"><%=rs("price")%></option>  
					<% 
						rs.movenext 
						loop 
						rs.close 	
					%>
					</select>
	      </div>
	    </div>
	    <button class="btn btn-inverse pull-right" type="reset">重置</button>
	    <button class="btn btn-success pull-right" type="submit">提交</button>
    </form> 
</div>
<!--#include file="./layout/adminFootLayout.asp"-->