<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span6 offset1 well">
	<form method="post" action="./savePWD.asp">
		<p class="text-error">修改密码</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
                  <span class="add-on">输入当前密码</span>
                  <input class="input" name="curPWD" type="password">
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
					<span class="add-on">请输入新密码</span>
					<input class="input" name="newPWD1" type="password">
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
					<span class="add-on">再次输入新密码</span>
					<input class="input" name="newPWD2" type="password">
                </div>
			</tr>
		</table>
		<button class="btn btn-success" type="submit" value="提交">提交</button>
		<button class="btn btn-inverse" type="reset" value="重置">重置</button>
	</form>
</div>
<!--#include file="./layout/staffFootLayout.asp"-->