<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">请选择要查询的季度</p>
		<p>若要查询三个季度以外的账单请至年度账单查询</p>
		<p>当前日期为<%=date()%></p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">查询今年的第</span>
					<select class="span2" name="season">
						<%
							curDate=date()
							season=Request.form("season")
							curDate=date()
							curMonth=month(curDate)
							curSeason=curMonth\4+1
							for i=1 to curSeason
								season_i=i
						%>
						<option><%=season_i%></option>
						<%next%>
					</select>
					<span class="add-on">季度</span>
					<button class="btn btn-mini btn-info" formaction="./querySBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
<%
	season=session("season")
	curDate=date()
	curYear=year(curDate)
	sumBalance=0
	if season=1 then
		startDate=DateSerial(curYear,1,1)
		endDate=DateSerial(curYear,3,31)
	end if
	if season=2 then
		startDate=DateSerial(curYear,4,1)
		endDate=DateSerial(curYear,6,30)
	end if
	if season=3 then
		startDate=DateSerial(curYear,7,1)
		endDate=DateSerial(curYear,9,30)
	end if
	if season=4 then
		startDate=DateSerial(curYear,10,1)
		endDate=DateSerial(curYear,12,31)
	end if
%>
<p>查询<%=curYear%>年第<%=season%>季度的账单信息</p>
<p class="text-info">起始日期：<%=startDate%>至<%=endDate%>的营业账单</p>
<%
	sql="select * from balance,repair,staff where balance_date between #"&startDate&" # and #"&endDate&"# and balance.balance_id=repair.balance_id and staff.usr_id=repair.usr_id"
	rs.open sql,conn,1,3
	if rs.eof then
        response.write ("<script>alert('没有查到指定时间内的账单信息!');</script>")
    else
    	do until rs.eof
		    balanceAmount=rs("balance.price")
			sumBalance=sumBalance+balanceAmount
		rs.movenext
		loop
		rs.close
		sql="select * from balance,repair,staff where balance_date between #"&startDate&" # and #"&endDate&"# and balance.balance_id=repair.balance_id and staff.usr_id=repair.usr_id"
		rs.open sql,conn,1,3
        rs.PageSize=1 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-info' href=./querySeasonBalanceResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-success' href=./querySeasonBalanceResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-success' href=./querySeasonBalanceResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-info' href=./querySeasonBalanceResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
        'For iPage = 1 To rs.PageSize
%>
          <table class="table table-striped table-bordered table-condensed">
            <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
            <th>账单编号</th>
			<th>账单日期</th>
			<th>账单状态</th>
			<th>账单费用</th>
			<th>维修编号</th>
			<th>维修费用</th>
			<th>维修日期</th>
			<th>维修状态</th>
			<th>维修人员</th>
			<th>维修人员联系方式</th>
			<th>配件及使用情况</th>
			<th>配件数量</th>
			<tr>
				<td><%Response.Write(rs("balance.balance_id"))%></td>
				<td><%Response.Write(rs("balance_date"))%></td>
				<td><%Response.Write(rs("balance.remark"))%></td>
				<td>
					<%
						Response.Write(rs("balance.price"))
					%>
				</td>
				<td><%Response.Write(rs("repair.repair_id"))%></td>
				<td><%Response.Write(rs("repair.price"))%></td>
				<td><%Response.Write(rs("repair_date"))%></td>
				<td><%Response.Write(rs("repair.remark"))%></td>
				<td><%Response.Write(rs("staff_name"))%></td>
				<td><%Response.Write(rs("contact"))%></td>
				<td colspan="2">
                    <table class="table">
                        <th>配件</th>
                        <th>配件数量</th>
                         <%		
                     		repairID=rs("repair.repair_id")
							set rs1=server.CreateObject("adodb.recordset")
							sql1="select * from use,parts where use.parts_id=parts.parts_id and use.repair_id="&repairID&""
							'response.write"'"&sql1&"'"
							rs1.open sql1,conn,1,3
							do until rs1.eof
                           %>
                      <tr>
                        <td><%Response.Write(rs1("parts_name"))%></td>
                        <td><%Response.Write(rs1("number"))%></td>
                      </tr>
                    <% 
                        rs1.MoveNext
                        loop
                        rs1.close
                    %>
                    </table>
                </td> 
<%
                	rs.MoveNext
              	end if
            Next
    end if
%>
</table>
<h3 class="text-error">阶段总营业额：<%=sumBalance%>￥</h3>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->