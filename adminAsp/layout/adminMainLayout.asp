<%@codepage="65001"%>
<%Session.CodePage=65001%>
<%
  userName=session("userName")
   if userName="" then 
    response.write"<script type='text/javascript' charset='gb2312'>alert('身份超时，请重新登录');location.href='./../index.asp'</script>"
  end if
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
    <title>Admin operations</title>
    <link rel="stylesheet" type="text/css" href="./../../html/css/main.css">
    <script type="text/javascript" src="./../../html/js/jquery.js"></script>
    <script type="text/javascript" src="./../../html/js/dropdown.js"></script>
    <script type="text/javascript" src="./../html/js/side.js"></script>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script type="text/javascript">  
        $(document).ready(function () {  
            $('.dropdown-toggle').dropdown();  
        });  
   </script>
</head>
<body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid">
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="brand" href="#">管理员工作界面</a>
        <div class="nav-collapse collapse">
          <ul class="nav pull-right">
            <p class="navbar-text pull-right">
              当前用户为 <a href="#" class="navbar-link"><%=session("userName")%></a>
            </p>
            <li class="divider-vertical"></li>
            <li class="dropdown" id="menutest1">
              <a class="dropdown-toggle pull-right" data-toggle="dropdown" href="#menutest1">
                <small>账号</small>
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="./changeUsrName.asp"><i class="icon-cog"></i><small>修改用户名</small></a></li>
                <li><a href="./changePWD.asp"><i class="icon-cog"></i><small>修改密码</small></a></li>
                <li class="divider"></li>
                <li><a href="./../index.asp"><i class="icon-circle-arrow-right"></i><small>退出</small></a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav nav-pills">
            <li class="active"><a href="./adminIndex.asp">主页</a></li>
            <li><a href="./../help.asp">帮助</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span3">
        <div class="well sidebar-nav">
          <ul class="nav nav-list">
            <%
              authority=session("authority")
              if authority="superAdmin" then
            %>
            <li class="nav-header">管理员管理</li>
            <li><a href="./newAdmin.asp" type="button">分配一个管理员账号</a></li>
            <li><a href="./queryAdmin.asp" type="button">查询管理员</a></li>
            <%
              end if
            %>
            <li class="nav-header">员工管理</li>
            <li><a href="./newStaff.asp" type="button">新建员工信息</a></li>
            <li><a href="./queryStaff.asp" type="button">员工信息查询修改</a></li>
            <li class="nav-header">营业查询</li>
            <li><a href="./queryWeekBalance.asp" type="button">周业绩查询</a></li>
            <li><a href="./queryMonthBalance.asp" type="button">月度营业查询</a></li>
            <li><a href="./querySeasonBalance.asp"type="button">季度营业查询</a></li>
            <li><a href="./queryYearBalance.asp"type="button">年度营业查询</a></li>
            <li class="nav-header">配件使用查询</li>
            <li><a type="button" href="./queryStore.asp">配件入库信息查询</a></li>
            <li><a type="button" href="./queryUse.asp">配件出售信息查询</a></li>
            <li><a type="button" href="./queryParts.asp">配件信息查询</a></li>
          </ul>
        </div><!--/.well -->
      </div><!--/span-->  
      