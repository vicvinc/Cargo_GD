<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span9 well">
    <p class="text-success">请选择账单信息</p>
    <form method="post" action="./queryBalanceCheck.asp">
        <div class="row-fluid">
          <div class="span3 offset2">
              <span>顾客姓名</span>
          <select name="customerName">
              <%
                sql="select * from customer"
                rs.open sql,conn,1,3
                do while not rs.eof 
          %>
            <option value="<%=rs("customer_name")%>"><%=rs("customer_name")%></option>  
          <% 
            rs.movenext 
            loop 
            rs.close  
          %>
          </select>
          </div>
          <div class="span3">
            <span>车牌号码</span>
          <select name="carLicense">
              <%
                sql="select * from car"
                rs.open sql,conn,1,3
                do while not rs.eof 
          %>
            <option value="<%=rs("license")%>"><%=rs("license")%></option>  
          <% 
            rs.movenext 
            loop 
            rs.close  
          %>
          </select>
          </div>
          <div class="span3">
              <span>维修人员</span>
            <select name="staffName">
                <%
                  sql="select * from staff"
                  rs.open sql,conn,1,3
                  do while not rs.eof 
            %>
              <option value="<%=rs("staff_name")%>"><%=rs("staff_name")%></option>  
            <% 
              rs.movenext 
              loop 
              rs.close  
            %>
            </select>
          </div>
        </div>
        <button class="btn btn-inverse pull-right" type="reset">重置</button>
        <button class="btn btn-success pull-right" type="submit">提交</button>
      </form> 
<%
      customerName=session("customerName")
      carLicense=session("carLicense")
      staffName=session("staffName")

      sql="select * from customer,car,staff,repair,balance where customer.customer_name='"&customerName&"' and customer.car_id=car.car_id and car.license='"&carLicense&"' and repair.car_id=car.car_id and repair.repair_id=balance.repair_id and repair.usr_id=staff.usr_id and staff.staff_name='"&staffName&"'"
      rs.open sql,conn,1,3
      response.write"<h4 class='text-warning'>查询结果</h4>"
      if rs.eof then
        response.write ("<script>alert('没有相关记录!');history.back();</script>")
      else
        repairID=rs("repair.repair_id")
        rs.PageSize=1 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-mini btn-info' href=./queryBalanceResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-mini btn-success' href=./queryBalanceResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-mini btn-success' href=./queryBalanceResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-mini btn-info' href=./queryBalanceResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-mini btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
%>
        <table class="table table-striped table-bordered table-condensed">
            <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
            <th>顾客编号</th>
            <th>顾客ID</th>
            <th>顾客姓名</th>
            <th>性别</th>
            <th>联系电话</th>
            <th>联系地址</th>
            <th>资料建立日期</th>
            <th>车辆名</th>
            <th>颜色</th>
            <th>车牌号码</th>
            <th>车辆型号</th>
            <th>故障简述</th>
            <th>操作</th>
            <tr>
              <td><%Response.Write(rs("customer.ID"))%></td>
              <td><%Response.Write(rs("customer.customer_id"))%></td>
              <td><%Response.Write(rs("customer_name"))%></td>
              <td><%Response.Write(rs("customer.gender"))%></td>
              <td><%Response.Write(rs("customer.contact"))%></td>
              <td><%Response.Write(rs("customer.address"))%></td>
              <td><%Response.Write(rs("cre_date"))%></td>
              <td><%Response.Write(rs("car_name"))%></td>
              <td><%Response.Write(rs("color"))%></td>
              <td><%Response.Write(rs("license"))%></td>
              <td><%Response.Write(rs("size"))%></td>
              <td><%Response.Write(rs("car.remark"))%></td>
              <td><%response.Write"<a class='btn btn-success btn-mini' href='./editCustomer.asp?id="&rs("customer.customer_id")&"'>修改</a><a class='btn btn-inverse btn-mini' href='./deleteCustomer.asp?id="&rs("customer.customer_id")&"'>删除</a>"%></td>
            </tr>
              <th>账单编号</th>
              <th>账单日期</th>
              <th>账单状态</th>
              <th>账单费用</th>
              <th>维修编号</th>
              <th>维修费用</th>
              <th>维修日期</th>
              <th>维修状态</th>
              <th>维修人员</th>
              <th>维修人员联系方式</th>
              <th colspan="2">配件及使用情况</th>
              <%
                balanceRemark=rs("balance.remark")
                if balanceRemark="已结算" then
              %>
              <td><%response.Write"<a class='btn btn-mini btn-inverse' type='submit' href='./settleBalance.asp?id="&rs("balance.balance_id")&"'>已结算</a>"%></td>
              <%else%>
              <td><%response.Write"<a class='btn btn-mini btn-info' type='submit' href='./settleBalance.asp?id="&rs("balance.balance_id")&"'>结算</a>"%></td>
              <%end if%>
            <tr>
                <td><%Response.Write(rs("balance.balance_id"))%></td>
                <td><%Response.Write(rs("balance_date"))%></td>
                <td><%Response.Write(rs("balance.remark"))%></td>
                <td><%Response.Write(rs("balance.price"))%></td>
                <td><%Response.Write(rs("repair.repair_id"))%></td>
                <td><%Response.Write(rs("repair.price"))%></td>
                <td><%Response.Write(rs("repair_date"))%></td>
                <td><%Response.Write(rs("repair.remark"))%></td>
                <td><%Response.Write(rs("staff_name"))%></td>
                <td><%Response.Write(rs("staff.contact"))%></td>
                <td colspan="2">
                    <table class="table">
                        <th>配件</th>
                        <th>配件数量</th>
                         <%
                              set rs1=server.CreateObject("adodb.recordset")
                              sql1="select * from use,parts where use.parts_id=parts.parts_id and use.repair_id="&repairID&""
                              'response.write"'"&sql1&"'"
                              rs1.open sql1,conn,1,3
                              do until rs1.eof
                            %>
                      <tr>
                        <td><%Response.Write(rs1("parts_name"))%></td>
                        <td><%Response.Write(rs1("number"))%></td>
                      </tr>
                    <% 
                        rs1.MoveNext
                        loop
                        rs1.close
                    %>
                    </table>
                </td> 
                <td>
                  <%response.Write"<a class='btn btn-success btn-mini' href='./editBalance.asp?id="&rs("balance.balance_id")&"'>修改</a>"%>
                </td>
            </tr>
<%
                rs.MoveNext
              end if
            Next
    end if
    rs.close
%>
        </table>
</div>
<!--#include file="./layout/staffFootLayout.asp"-->