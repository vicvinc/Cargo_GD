<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span9 well">
	<p class="text-success">请选择账单信息</p>
	<form method="post" action="./queryBalanceCheck.asp">
	    <div class="row-fluid">
	      <div class="span3 offset2">
	      		<span>顾客姓名</span>
				<select name="customerName">
	      		<%
		      		sql="select * from customer"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("customer_name")%>"><%=rs("customer_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
	      </div>
	      <div class="span3">
	      	<span>车牌号码</span>
				<select name="carLicense">
	      		<%
		      		sql="select * from car"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("license")%>"><%=rs("license")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
	      </div>
	      <div class="span3">
		      	<span>维修人员</span>
					<select name="staffName">
		      		<%
			      		sql="select * from staff"
			      		rs.open sql,conn,1,3
			      		do while not rs.eof 
					%>
						<option value="<%=rs("staff_name")%>"><%=rs("staff_name")%></option>  
					<% 
						rs.movenext 
						loop 
						rs.close 	
					%>
					</select>
	      </div>
	    </div>
	    <button class="btn btn-inverse pull-right" type="reset">重置</button>
	    <button class="btn btn-success pull-right" type="submit">提交</button>
    </form> 
</div>
<!--#include file="./layout/staffFootLayout.asp"-->