<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="well span9">
	<p class="text-success">请选择账单信息</p>
	<form method="post" action="./checkBalance.asp">
    <div class="row-fluid">
      <div class="span3">
      		<span>顾客姓名</span>
			<select name="customerName">
      		<%
	      		sql="select * from customer"
	      		rs.open sql,conn,1,3
	      		do while not rs.eof 
			%>
				<option value="<%=rs("customer_name")%>"><%=rs("customer_name")%></option>  
			<% 
				rs.movenext 
				loop 
				rs.close 	
			%>
			</select>
			<span>车牌号码</span>
			<select name="carLicense">
      		<%
	      		sql="select * from car"
	      		rs.open sql,conn,1,3
	      		do while not rs.eof 
			%>
				<option value="<%=rs("license")%>"><%=rs("license")%></option>  
			<% 
				rs.movenext 
				loop 
				rs.close 	
			%>
			</select>
      </div>
      <div class="span3">
	      	<span>维修人员</span>
				<select name="staffName">
	      		<%
		      		sql="select * from staff"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("staff_name")%>"><%=rs("staff_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
      </div>
      <div class="span3">
      	<span>配件信息</span>
			<select name="partsName">
	      		<%
		      		sql="select * from parts"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("parts_name")%>"><%=rs("parts_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 
				'ee=Trim(Request.Form("ee"))	
				%>
			</select>
		<span>数量</span>
		<input class="input" name="partsNum" type="text" placeholder="配件数量">
      </div>
      <div class="span3">
      	<span>维修费用</span>
		<input class="input" name="repairFee" type="text" placeholder="维修费用">
      </div>
    </div>
    <button class="btn btn-success" type="submit">提交</button>
    <button class="btn btn-inverse" type="reset">重置</button>
    </form> 
</div>
<!--#include file="./layout/staffFootLayout.asp"-->