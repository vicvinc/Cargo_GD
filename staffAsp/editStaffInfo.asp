<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	userName=session("userName")
	sql="select * from staff,staffUsr where staffUsr.usr_name='"&userName&"' and staffUsr.usr_id=staff.usr_id"
	rs.open sql,conn,1,1
	session("staffID")=rs("staff.usr_id")
%>
<div class="span6 offset1 well">
	<form method="post" action="./updateStaff.asp">
		<p class="text-error">个人信息</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
                  <span class="add-on">姓名</span>
                  <input class="input" name="staffName" type="text" value="<%=rs("staff_name")%>">
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
					<span class="add-on">性别</span>
					<select name="staffGender">
						<option><%=rs("gender")%></option>
						<%
						if rs("gender")="男" then
						%>
						<option>女</option>
						<%else%>
						<option>男</option>
						<%end if%>
					<select>
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
                  <span class="add-on">联系电话</span>
                  <input class="input" name="staffPhone" type="text" value="<%=rs("contact")%>">
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
					<span class="add-on">联系地址</span>
					<input class="input" name="staffAddress" type="text" value="<%=rs("address")%>">
                </div>
			</tr>
			<tr>
				<div class="input-prepend">
					<span class="add-on">年龄</span>
					<input class="input" name="staffAge" type="text" value="<%=rs("age")%>">
                </div>
			</tr>
		</table>
		<button class="btn btn-success" type="submit" value="提交">提交</button>
		<button class="btn btn-inverse" type="reset" value="重置">重置</button>
	</form>
</div>
<%rs.close%>
<!--#include file="./layout/staffFootLayout.asp"-->