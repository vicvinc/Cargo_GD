<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span9 well">
	<p class="text-success">请选择配件信息</p>
	<form method="post" action="./queryPartsCheck.asp">
	    <div class="row-fluid">
	      <div class="span3 offset2">
	      		<span>配件名</span>
				<select name="partsName">
	      		<%
		      		sql="select * from parts"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("parts_name")%>"><%=rs("parts_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
	      </div>
	      <div class="span3">
	      	<span>供应商</span>
				<select name="supplierName">
	      		<%
		      		sql="select * from supplier"
		      		rs.open sql,conn,1,3
		      		do while not rs.eof 
				%>
					<option value="<%=rs("supplier_name")%>"><%=rs("supplier_name")%></option>  
				<% 
					rs.movenext 
					loop 
					rs.close 	
				%>
				</select>
	      </div>
	      <div class="span3">
		      	<span>配件价格</span>
					<select name="partsPrice">
		      		<%
			      		sql="select * from parts"
			      		rs.open sql,conn,1,3
			      		do while not rs.eof 
					%>
						<option value="<%=rs("price")%>"><%=rs("price")%></option>  
					<% 
						rs.movenext 
						loop 
						rs.close 	
					%>
					</select>
	      </div>
	    </div>
	    <button class="btn btn-inverse pull-right" type="reset">重置</button>
	    <button class="btn btn-success pull-right" type="submit">提交</button>
    </form>
    <%

	  partsName=session("partsName")
	  partsPrice=session("partsPrice")
	  supplierName=session("supplierName")

	  sql="select * from parts,supplier where "
	  if partsName<>"" then
	    sql=sql+"parts_name='"&partsName&"' and "
	  end if
	  if partsPrice<>"" then
	    sql=sql+"price="&partsPrice&" and "
	  end if
	  if supplierName<>""then
	    sql=sql+"supplier_name='"&supplierName&"' and "
	  end if
	  sql=sql+"supplier.supplier_id=parts.supplier_id"

	  rs.open sql,conn
	  'Response.Write"'"&sql&"'"
	  if rs.eof then
	  	Response.Write"<script>alert('未查到相关信息');</script>"
	  else
	  	rs.PageSize=3 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-mini btn-info' href=./queryCustomerResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-mini btn-success' href=./queryCustomerResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-mini btn-success' href=./queryCustomerResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-mini btn-info' href=./queryCustomerResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-mini btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
%>
	<p class="text-success">查询结果</p>
	<table class="table">
	    <th>配件编号</th>
	    <th>配件ID</th>
	    <th>配件名</th>
	    <th>配件价格</th>
	    <th>配件备注信息</th>
		<th>供应商编号</th>
	    <th>供应商</th>
	    <th>联系电话</th>
	    <th>联系地址</th>
	    <th>资料建立日期</th>
	    <th>公司</th>
	    <th>库存</th>
	    <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
	      <tr>
	          <td><%Response.Write(rs("parts.ID"))%></td>
	          <td><%Response.Write(rs("parts_id"))%></td>
	          <td><%Response.Write(rs("parts_name"))%></td>
	          <td><%Response.Write(rs("price"))%></td>
	          <td><%Response.Write(rs("remark"))%></td>
	          <td><%Response.Write(rs("supplier.ID"))%></td>
	          <td><%Response.Write(rs("supplier_name"))%></td>
	          <td><%Response.Write(rs("contact"))%></td>
	          <td><%Response.Write(rs("address"))%></td>
	          <td><%Response.Write(rs("supplyDate"))%></td>
	          <td><%Response.Write(rs("company"))%></td>
	          <td><%
			          set rs1=server.CreateObject("adodb.recordset")
			          sql="select * from storage where parts_id="&rs("parts_id")&""
			          rs1.open sql,conn,1,3
			          if rs1.eof then
			          	Response.Write 0
			          else
			          	Response.Write(rs1("number"))
			          end if
	          %></td>
	      </tr>
		<% rs.MoveNext
              end if
            Next
    	end if
    	rs.close
    	%>
	</table>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->