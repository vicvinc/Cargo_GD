<!--#include file="./layout/staffMainLayout.asp"-->
	  <div class="span7 offset1 success">
            <form  class="" method="post" action="./saveCustomer.asp">
              <fieldset>
                <p class="text-success">建立新用户信息</p>
                <table class="table">
                  <tr>
                    <td>
                      <table class="table">
                        <tr>
                          <h4>新用户信息</h4>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>用户名称</span>
                              <input name="customer_name" type="text" placeholder="用户名">
                            </div>  
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>用户性别</span>
                              <select name="customer_gender" placeholder="性别">
                                <option>男</option>
                                <option>女</option>
                              </select>
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>联系电话</span>
                              <input class="input" name="customer_contact" type="text" placeholder="联系电话">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">联系地址</span>
                              <input class="input" name="contact_addr" type="text" placeholder="联系地址">
                            </div>
                        </tr>
                      </table>
                    </td>
                    <td>
                      <table class="table table-hover">
                        <tr>
                          <h4>用户车辆信息</h4>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">车名</span>
                              <input class="input" name="car_name" type="text" placeholder="车名">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>牌照</span>
                              <input class="input" name="car_license" type="text" placeholder="牌照">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">颜色</span>
                              <input class="input" name="car_color" type="text" placeholder="颜色">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">型号</span>
                              <input class="input" name="car_size" type="text" placeholder="型号">
                            </div>
                        </tr>
                      </table>
                    </td>
                  </tr>
                </table>
                <div>
                  <p><font color="red">***</font>车辆故障备注信息:</p>
                  <textarea class="span7" name="car_trouble"></textarea>
                </div>
                <input type="submit" class="btn-success" value="提交">
                <input type="reset" class="btn-inverse" value="重置">
              </fieldset>
            </form>
        </div><!--/span-->
<!--#include file="./layout/staffFootLayout.asp"-->