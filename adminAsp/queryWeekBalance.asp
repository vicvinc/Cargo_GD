<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">查询最近三周内营业情况</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">请选择最近的周次</span>
					<select name="week">
						<option>1</option>
						<option>2</option>
						<option>3</option>
					</select>
					<button class="btn btn-mini btn-info" formaction="./queryWBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->