<!--#include file="./layout/staffMainLayout.asp"-->
	<div class="span9">
    <div class="hero-unit">
      <h3>欢迎来到员工管理界面！</h3>
      <p>在这里您可以通过左侧的标题栏来管理您的日常工作</p>
    </div>
    <div class="row-fluid">
      <div class="span4">
        <h3>用户管理</h3>
        <p>在这里可以为新用户建立用户和用户车辆资料，可以通过查询来修改用户和用户车辆的资料</p>
      </div><!--/span-->
      <div class="span4">
        <h3>账单管理</h3>
        <p>这里可以为用户建立新的用户维修使用账单，查询和修改用户账单信息，结算用户账单，并且打印相关用户账单信息</p>
      </div><!--/span-->
      <div class="span4">
        <h3>配件管理</h3>
        <p>这里可以通过供应商添加新的配件到库存，记录配件入库信息，查询和修改配件信息，配件库存信息查询</p>
      </div><!--/span-->
    </div><!--/row-->
</div><!--/span-->
<!--#include file="./layout/staffFootLayout.asp"-->