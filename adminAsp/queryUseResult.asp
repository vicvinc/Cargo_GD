<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<p class="text-success">配件售出查询</p>
    <p>（若查询信息为空，默认查询所有配件售出情况）</p>
    <form class="form-inline" method="post" action="./queryUseCheck.asp">
        <span>配件名</span>
        <input class="input-small" name="partsName" type="text" placeholder="配件名">
        <span>存储日期</span>
        <input class="input-small" name="useDate" type="text" placeholder="售出日期">
        <span>价格</span>
        <input class="input-small" name="partsPrice" type="text" placeholder="价格">
        <span>供应商</span>
        <input class="input-small" name="supplierName" type="text" placeholder="供应商">
        <button type="submit" class="btn btn-small btn-info">查询</button>
     </form>
	<p class="text-success">查询结果</p>
<%
	partsName=session("partsName")
	useDate=session("useDate")
	partsPrice=session("partsPrice")
	supplierName=session("supplierName")

	sql="select * from use,parts,supplier,repair,car,customer,staff,storage where "
	if partsName<>"" then
		sql=sql+"parts.parts_name='"&partsName&"' and "
	end if
	if storeDate<>"" then
		sql=sql+"use.useDate='"&useDate&"' and "
	end if
	if partsPrice<>"" then
		sql=sql+"parts.price='"&partsPrice&"' and "
	end if
	if supplierName<>"" then
		sql=sql+"supplier.supplier_name='"&supplierName&"' and "
	end if
	sql=sql+"parts.supplier_id=supplier.supplier_id and parts.parts_id=use.parts_id and use.repair_id=repair.repair_id and repair.car_id=car.car_id and car.car_id=customer.car_id and repair.usr_id=staff.usr_id and storage.parts_id=parts.parts_id"
	rs.open sql,conn,1,3
  	'Response.Write"'"&sql&"'"
	  if rs.eof then
	  	Response.Write"<script>alert('未查到相关信息');</script>"
	  else
	  	rs.PageSize=1 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-mini btn-info' href=./queryUseResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-mini btn-success' href=./queryUseResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-mini btn-success' href=./queryUseResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-mini btn-info' href=./queryUseResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-mini btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
%>
	<table class="table table-hover">
	    <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
	    <th>配件编号</th>
	    <th>配件ID</th>
	    <th>配件名</th>
	    <th>配件价格</th>
	    <th>配件备注信息</th>
		<th>供应商编号</th>
	    <th>供应商</th>
	    <th>联系电话</th>
	    <tr>
			<td><%Response.Write(rs("parts.ID"))%></td>
			<td><%Response.Write(rs("parts.parts_id"))%></td>
			<td><%Response.Write(rs("parts_name"))%></td>
			<td><%Response.Write(rs("parts.price"))%></td>
			<td><%Response.Write(rs("parts.remark"))%></td>
			<td><%Response.Write(rs("supplier.ID"))%></td>
			<td><%Response.Write(rs("supplier_name"))%></td>
			<td><%Response.Write(rs("supplier.contact"))%></td>
	    </tr>
	    <th>顾客</th>
	    <th>联系电话</th>
	    <th>使用编号</th>
	    <th><p class="text-error">售出数量</p></th>
	    <th>售出日期</th>
	    <th>库存编号</th>
	    <th>库存剩余数量</th>
	    <tr>
			<td><%Response.Write(rs("customer_name"))%></td>
			<td><%Response.Write(rs("customer.contact"))%></td>
			<td><%Response.Write(rs("use.ID"))%></td>
			<td><p class="text-error"><%Response.Write(rs("use.number"))%></p></td>
			<td><%Response.Write(rs("useDate"))%></td>
			<td><%Response.Write(rs("storage_id"))%></td>
			<td><%Response.Write(rs("storage.number"))%></td>
	    </tr>
		<% rs.MoveNext
              end if
            Next
    	end if
    	rs.close
    	%>
	</table>
</div>

<!--#include file="./layout/adminFootLayout.asp"-->