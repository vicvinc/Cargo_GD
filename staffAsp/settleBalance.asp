<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->

<script>
    function myPrint(obj){
        var newWindow=window.open("打印窗口","_blank");
        var docStr = obj.innerHTML;
        newWindow.document.write(docStr);
        newWindow.document.close();
        newWindow.print();
        newWindow.close();
    }
</script>
<div id="print" class="span8 well">
<%
    balanceID=request("id")
    sql="select * from repair,balance,car,customer,staff where balance.balance_id="&balanceID&" and balance.balance_id=repair.balance_id and repair.car_id=car.car_id and car.car_id=customer.car_id and repair.usr_id=staff.usr_id"
    'response.write"'"&sql&"'"
    rs.open sql,conn,1,3
      response.write"<h4 class='text-warning'>账单信息</h4>"
      if rs.eof then
        response.write ("<script>alert('结算出错!');history.back();</script>")
      else
        repairID=rs("repair.repair_id")
        rs.PageSize=1 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-mini btn-info' href=./queryBalanceResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-mini btn-success' href=./queryBalanceResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-mini btn-success' href=./queryBalanceResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-mini btn-info' href=./queryBalanceResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-mini btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
        'For iPage = 1 To rs.PageSize
%>
        <table name="print" class="table table-hover table-striped table-bordered table-condensed">
            <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
            <th>顾客编号</th>
            <th>顾客ID</th>
            <th>顾客姓名</th>
            <th>性别</th>
            <th>联系电话</th>
            <th>联系地址</th>
            <th>资料建立日期</th>
            <th>车辆名</th>
            <th>颜色</th>
            <th>车牌号码</th>
            <th>车辆型号</th>
            <th>故障简述</th>
            <tr>
              <td><%Response.Write(rs("customer.ID"))%></td>
              <td><%Response.Write(rs("customer.customer_id"))%></td>
              <td>
                <%
                    customerName=rs("customer_name")
                    Response.Write(rs("customer_name"))
                %>
            </td>
              <td><%
                    customerGender=rs("customer.gender")
                    Response.Write(rs("customer.gender"))
                    %>
                </td>
              <td><%
                    customerPhone=rs("customer.contact")
                    Response.Write(rs("customer.contact"))
                    %>
                </td>
              <td><%
                    customerAddress=rs("customer.address")
                    Response.Write(rs("customer.address"))
                    %>
                </td>
              <td><%Response.Write(rs("cre_date"))%></td>
              <td><%Response.Write(rs("car_name"))%></td>
              <td><%Response.Write(rs("color"))%></td>
              <td><%Response.Write(rs("license"))%></td>
              <td><%Response.Write(rs("size"))%></td>
              <td><%
                    carTrouble=rs("car.remark")
                    Response.Write(rs("car.remark"))
                    %>
                </td>
            </tr>
              <th>账单编号</th>
              <th>账单日期</th>
              <th>账单状态</th>
              <th>维修编号</th>
              <th>维修日期</th>
              <th>维修状态</th>
              <th>维修人员</th>
              <th>维修人员联系方式</th>
              <th colspan="4">配件及使用情况</th>
            <tr>
                <td><%Response.Write(rs("balance.balance_id"))%></td>
                <td><%Response.Write(rs("balance_date"))%></td>
                <td><%
                    balanceRemark="已结算"
                    rs("balance.remark")=balanceRemark
                    Response.Write(rs("balance.remark"))

                    %>
                </td>
                <td><%Response.Write(rs("repair.repair_id"))%></td>
                <td><%Response.Write(rs("repair_date"))%></td>
                <td><%
                    repairRemark="修理完成"
                    rs("repair.remark")=repairRemark
                    Response.Write(rs("repair.remark"))%></td>
                <td><%
                    staffName=rs("staff_name")
                    Response.Write(rs("staff_name"))%></td>
                <td><%Response.Write(rs("staff.contact"))%></td>
                <td colspan="4">
                    <table class="table">
                        <th>配件</th>
                        <th>数量</th>
                        <th>单价</th>
                         <% 

                              set rs1=server.CreateObject("adodb.recordset")
                              sql1="select * from use,parts where use.parts_id=parts.parts_id and use.repair_id="&repairID&""
                              'response.write"'"&sql1&"'"
                              partsFee=0
                              rs1.open sql1,conn,1,3
                              do until rs1.eof
                                partPrice=rs1("price")
                                partsNum=rs1("number")
                                partsFee=partsFee+partPrice*partsNum
                            %>
                      <tr>
                        <td><%Response.Write(rs1("parts_name"))%></td>
                        <td><%Response.Write(rs1("number"))%></td>
                        <td><%Response.Write(rs1("price"))%></td>
                      </tr>
                    <% 
                        rs1.MoveNext
                        loop
                        rs1.close
                    %>
                    </table>
                </td> 
            </tr>
            <th>维修费用</th>
            <th>配件费用</th>
            <th>账单费用</th>
            <tr>
                <td>
                    <%
                        repairFee=rs("repair.price")
                        Response.Write(rs("repair.price"))
                    %>
                </td>
                <td><%=partsFee%></td>
                <td>
                    <%
                        balanceFee=partsFee+repairFee
                        rs("balance.price")=balanceFee
                        Response.Write(rs("balance.price"))
                    %>
                </td>
            </tr>
<%              
                
                rs.update()
                rs.MoveNext
              end if
            Next
    end if
    rs.close
%>
    </table>
    <div id="myModal" class="modal hide fade" style="display: none;">
        <div class="modal-header">
            <a class="close" data-dismiss="modal">×</a>
            <h3>发票信息</h3>
        </div>
            
        <div id="print" name="print" class="modal-body">
            <p class="text-success">维修费用结算凭据</p>
            <table class="table table-hover table-striped table-bordered table-condensed">
                <th>账单编号</th>
                <th>结算日期</th>
                <th>顾客姓名</th>
                <th>联系电话</th>
                <th>维修故障</th>
                <th>维修人员</th>
                <tr class="info">
                    <td><%=balanceID%></td>
                    <td><%=date()%></td>
                    <td><%=customerName%></td>
                    <td><%=customerPhone%></td>
                    <td><%=carTrouble%></td>
                    <td><%=staffName%></td>
                </tr>
                <th>维修费用</th>
                <th>配件费用</th>
                <th>账单费用</th>
                <tr class="success">
                    <td><%=repairFee%></td>
                    <td><%=partsFee%></td>
                    <td><%=balanceFee%></td>
                </tr>
            </table>
          <h4>结算账单</h4>
          <p class="text-error">结算账单总的费用是 "维修费用" + "配件费用" 所得，若有疑问请与负责维修的员工联系！</p>
        </div>
        <div class="modal-footer noPrint">
          <a class="btn" data-dismiss="modal">关闭</a>
          <a class="btn btn-primary" onclick="myPrint(document.getElementById('print'))">打印发票</a>
        </div>
    </div>
    <a data-toggle="modal" href="#myModal" class="btn btn-primary btn-block noPrint">查看发票</a>
</div>
<!--#include file="./layout/staffFootLayout.asp"-->