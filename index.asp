<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>new Home</title>
    <link href="./html/css/main.css" rel="stylesheet">
    <link href="./html/css/index.css" rel="stylesheet">
    <script src="./html/js/jquery.js"></script>
    <script type="text/javascript" src="./html/js/cookie.js"></script>  
    <script type="text/javascript" src="./html/js/common.js"></script>  
    <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
    <!--[if lt IE 9]>
      <script src="../assets/js/html5shiv.js"></script>
    <![endif]-->
    <!-- Fav and touch icons -->
    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #f5f5f5;
      }

      .form-signin {
        max-width: 300px;
        padding: 19px 29px 29px;
        margin: 0 auto 20px;
        background-color: #fff;
        border: 1px solid #e5e5e5;
        -webkit-border-radius: 5px;
           -moz-border-radius: 5px;
                border-radius: 5px;
        -webkit-box-shadow: 0 1px 2px rgba(0,0,0,.05);
           -moz-box-shadow: 0 1px 2px rgba(0,0,0,.05);
                box-shadow: 0 1px 2px rgba(0,0,0,.05);
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin input[type="text"],
      .form-signin input[type="password"] {
        font-size: 16px;
        height: auto;
        margin-bottom: 15px;
        padding: 7px 9px;
      }
    </style>
</head>
  <body>
    
    <div class="container">
      <div class="row">
        <div class="span6 offset3">
          <div>
            <h3>“车爵士”汽车修理管理系统<small>for</small></h3>
            <p>小型的专业的汽车维修店,利用ASP语言和sq数据库管理系统等开发实现的汽车维修管理系统，简单轻便，利于用户的使用和操作.</p>
          </div>
          <form class="form-signin" method="post" action="./login.asp">
            <h2 class="form-signin-heading">请登录</h2>
            <input type="text" class="input" placeholder="用户名" name="userName" id="userName">
            <input name="password" type="password" id="password" class="input" placeholder="密码">
            <label class="checkbox">
              <input name="identity" type="radio" value="staff">员工
              <input name="identity" type="radio" value="admin">管理员
            </label>
            <p class="text-info">   
              <input id="saveCookie" type="checkbox" value="" />
              <span>记住密码</span> 
            </p> 
            <button id="submit" class="btn btn-large btn-success" type="submit" name="Submit" value="提交">登录</button>
          </form>
        </div>
      </div><!-- /.row -->
    </div>
           
    <footer>
      <div class="container">
        <div class="row-fluid">
          <div class="span3 offset3">
            <p>Design BY M.TFSU © Graduate 2013</p>
          </div>
        </div>
      </div>
    </footer>
</body>
</html>