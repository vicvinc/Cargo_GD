<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span9 well">
    <p class="text-success">配件出售查询</p>
    <p>（若查询信息为空，默认查询所有配件入库信息）</p>
    <form class="form-inline" method="post" action="./queryUseCheck.asp">
        <span>配件名</span>
        <input class="input-small" name="partsName" type="text" placeholder="配件名">
        <span>售出日期</span>
        <input class="input-small" name="useDate" type="text" placeholder="创建日期">
        <span>价格</span>
        <input class="input-small" name="partsPrice" type="text" placeholder="价格">
        <span>供应商</span>
        <input class="input-small" name="supplierName" type="text" placeholder="供应商">
        <button type="submit" class="btn btn-small btn-info">查询</button>
     </form>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->