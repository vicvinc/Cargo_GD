<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	dim cus_name,cus_gender,cus_contact,cus_addr,cre_date,car_name,car_license,car_color,car_size,car_trouble,cus_id,car_id
	cus_name=request.Form("customer_name")
	cus_gender=request.form("customer_gender")
	cus_contact=request.form("customer_contact")
	cus_addr=request.form("contact_addr")
	cre_date=date() 
	car_name=request.form("car_name")
	car_license=request.form("car_license")
	car_color=request.form("car_color")
	car_size=request.form("car_size")
	car_trouble=request.form("car_trouble")
	
	sql2="select max(customer_id) from customer"
	rs.open sql2,conn,1,3
	cus_id=rs.Fields("Expr1000")
	if isnull(cus_id) then
		cus_id=1
	else
		cus_id=cus_id+1
	end if
	rs.close

	if cus_name=""then
		response.write ("<script>alert('用户名不为空!');history.back();</script>")
	else
		if cus_gender=""then
			response.write ("<script>alert('请选择用户性别');history.back();</script>")
		else
			if car_license=""then
				response.write ("<script>alert('请填写汽车牌照');history.back();</script>")
			else
				if car_trouble=""then 
					response.write ("<script>alert('请填写故障描述');history.back();</script>")
				else
					sql1="select * from customer" 
					rs.open sql1,conn,1,3
					rs.addnew()
					rs("customer_id")=cus_id
					rs("car_id")=cus_id
					rs("customer_name")=cus_name
					rs("gender")=cus_gender
					rs("contact")=cus_contact
					rs("address")=cus_addr
					rs("cre_date")=cre_date
					rs.update()
					rs.close

					sql3="select * from car"
					rs.open sql3,conn
					car_id=cus_id
					rs.addnew
					rs("customer_id")=cus_id
					rs("car_id")=cus_id 
					rs("license")=car_license
					rs("size")=car_size
					rs("color")=car_color
					rs("car_name")=car_name
					rs("remark")=car_trouble
					
					rs.update()
					rs.close
				end if
			end if
		end if
	end if

	sql4="select * from customer,car where customer.customer_id="&cus_id&" and customer.customer_id=car.customer_id" 
	rs.open sql4,conn,1,3
%>
<div class="span9">
	<p class="text-success">新建的用户信息</p>
	<table class="table">
	    <th>顾客编号</th>
	    <th>顾客ID</th>
	    <th>顾客姓名</th>
	    <th>性别</th>
	    <th>联系电话</th>
	    <th>联系地址</th>
	    <th>资料建立日期</th>
	    <th>车辆名</th>
	    <th>颜色</th>
	    <th>车牌号码</th>
	    <th>车辆型号</th>
	    <th>故障简述</th>
	      <tr>
	          <td><%Response.Write(rs("customer.ID"))%></td>
	          <td><%Response.Write(rs("customer.customer_id"))%></td>
	          <td><%Response.Write(rs("customer_name"))%></td>
	          <td><%Response.Write(rs("gender"))%></td>
	          <td><%Response.Write(rs("contact"))%></td>
	          <td><%Response.Write(rs("address"))%></td>
	          <td><%Response.Write(rs("cre_date"))%></td>
	          <td><%Response.Write(rs("car_name"))%></td>
	          <td><%Response.Write(rs("color"))%></td>
	          <td><%Response.Write(rs("license"))%></td>
	          <td><%Response.Write(rs("size"))%></td>
	          <td><%Response.Write(rs("remark"))%></td>
	      </tr>
		<%rs.close%>
	</table>
</div>
<!--#include file="./layout/staffFootLayout.asp"-->