<!--#include file="./layout/staffMainLayout.asp"-->
		<div class="span7 offset1 success">
	        <p class="text-success">选择要查询的用户</p>
	        <small class="text-warning">请至少填写一个查询信息</small>
	        <form class="form-inline" method="post" action="./queryCustomerCheck.asp">
	            <span>用户姓名</span>
	            <input class="input-small" name="customer_name" type="text" placeholder="用户名">
	            <span>创建日期</span>
	            <input class="input-small date-pick" name="cre_date" type="text" placeholder="创建日期">
	            <span>牌照</span>
	            <input class="input-small" name="car_license" type="text" placeholder="牌照">
	            <button type="submit" class="btn btn-small btn-info">查询</button>
	         </form>
        </div>
<!--#include file="./layout/staffFootLayout.asp"-->