<%@codepage="65001"%>
<%Session.CodePage=65001%>
<%
  userName=session("userName")
  if userName="" then 
    response.write"<script type='text/javascript' charset='gb2312'>alert('身份超时，请重新登录');location.href='./../index.asp'</script>"
  end if
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=Utf-8">
    <title>Staff operations</title>
    <link rel="stylesheet" type="text/css" href="./../../html/css/main.css">
    <link rel="stylesheet" type="text/css" href="./../../html/css/datepicker.css">
    <script type="text/javascript" src="./../../html/js/jquery.js"></script>
    <script type="text/javascript" src="./../../html/js/dropdown.js"></script>
    <script type="text/javascript" src="./../../html/js/model.js"></script>
    <script type="text/javascript" src="./../../html/js/transitions.js"></script>
    <script type="text/javascript" src="./../../html/js/tooltip.js"></script>
    <script type="text/javascript" src="./../../html/js/popovers.js"></script>
    <script type="text/javascript" src="./../../html/js/datepicker.js"></script>
    <style type="text/css">
      body {
        padding-top: 60px;
        padding-bottom: 40px;
      }
      .sidebar-nav {
        padding: 9px 0;
      }
      @media (max-width: 980px) {
        /* Enable use of floated navbar text */
        .navbar-text.pull-right {
          float: none;
          padding-left: 5px;
          padding-right: 5px;
        }
      }
    </style>
    <script type="text/javascript">  
        $(document).ready(function () {  
            $('.dropdown-toggle').dropdown();
            $('.date-pick').datePicker();  
        }); 
   </script>
</head>
<body>
  <div class="navbar navbar-fixed-top">
    <div class="navbar-inner">
      <div class="container-fluid">
        <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="brand" href="#">员工工作管理界面</a>
        <div class="nav-collapse collapse">
          <ul class="nav pull-right">
            <p class="navbar-text pull-right">
              当前用户为 <a href="./staffInfo.asp" class="navbar-link"><%=session("userName")%></a>
            </p>
            <li class="divider-vertical"></li>
            <li class="dropdown" id="menutest1">
              <a class="dropdown-toggle pull-right" data-toggle="dropdown" href="#menutest1">
                <small>账号</small>
                <b class="caret"></b>
              </a>
              <ul class="dropdown-menu">
                <li><a href="./changeUsrName.asp"><i class="icon-cog"></i><small>修改用户名</small></a></li>
                <li><a href="./changePWD.asp"><i class="icon-cog"></i><small>修改密码</small></a></li>
                <li><a href="./editStaffInfo.asp"><i class="icon-cog"></i><small>编辑个人信息</small></a></li>
                <li class="divider"></li>
                <li><a href="./../index.asp"><i class="icon-circle-arrow-right"></i><small>退出</small></a></li>
              </ul>
            </li>
          </ul>
          <ul class="nav nav-pills">
            <li class="active"><a href="./staffIndex.asp">主页</a></li>
            <li><a href="./../help.asp">帮助</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </div>
  </div>
  <div class="container-fluid">
    <div class="row-fluid">
      <div class="span3 bs-docs-sidebar">
        <ul class="nav nav-list">
          <li class="nav-header">用户管理</li>
          <li><a href="./newCustomer.asp" type="button">新建用户<i class="icon-chevron-right"></i></a></li>
          <li><a href="./queryCustomer.asp" type="button">用户信息查询修改<i class="icon-chevron-right"></i></a></li>
          <li class="nav-header">账单管理</li>
          <li><a href="./newBalance.asp" type="button">新建账单<i class="icon-chevron-right"></i></a></li>
          <li><a href="./queryBalance.asp" type="button">账单查询修改结算<i class="icon-chevron-right"></i></a></li>
          <li class="nav-header">配件管理</li>
          <li><a type="button" href="./newParts.asp">新建配件信息<i class="icon-chevron-right"></i></a></li>
          <li><a type="button" href="./queryParts.asp">配件信息查询修改<i class="icon-chevron-right"></i></a></li>
          <li><a type="button" href="./storeParts.asp">配件入库管理<i class="icon-chevron-right"></i></a></li>
        </ul>
      </div><!--/span--> 

      