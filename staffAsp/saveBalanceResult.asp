<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	sql=session("queryBalanceSql")
	rs.open sql,conn,1,3
%>
<div class="span9">
	<p class="text-success">新建的账单信息</p>
	<table class="table">
	    <th>顾客编号</th>
	    <th>顾客ID</th>
	    <th>顾客姓名</th>
	    <th>性别</th>
	    <th>联系电话</th>
	    <th>联系地址</th>
	    <th>资料建立日期</th>
	    <th>车辆名</th>
	    <th>颜色</th>
	    <th>车牌号码</th>
	    <th>车辆型号</th>
	    <th>故障简述</th>
	      <tr>
	          <td><%Response.Write(rs("customer.ID"))%></td>
	          <td><%Response.Write(rs("customer.customer_id"))%></td>
	          <td><%Response.Write(rs("customer_name"))%></td>
	          <td><%Response.Write(rs("customer.gender"))%></td>
	          <td><%Response.Write(rs("customer.contact"))%></td>
	          <td><%Response.Write(rs("customer.address"))%></td>
	          <td><%Response.Write(rs("cre_date"))%></td>
	          <td><%Response.Write(rs("car_name"))%></td>
	          <td><%Response.Write(rs("color"))%></td>
	          <td><%Response.Write(rs("license"))%></td>
	          <td><%Response.Write(rs("size"))%></td>
	          <td><%Response.Write(rs("car.remark"))%></td>
	      </tr>
		<th>账单编号</th>
		<th>账单日期</th>
		<th>账单状态</th>
		<th>账单费用</th>
		<th>维修编号</th>
		<th>维修费用</th>
		<th>维修日期</th>
		<th>维修状态</th>
		<th>维修人员</th>
		<th>维修人员联系方式</th>
		<th>配件及使用情况</th>
		<th>配件数量</th>
		<tr>
			<td><%Response.Write(rs("balance.balance_id"))%></td>
			<td><%Response.Write(rs("balance_date"))%></td>
			<td><%Response.Write(rs("balance.remark"))%></td>
			<td><%Response.Write(rs("balance.price"))%></td>
			<td><%Response.Write(rs("repair.repair_id"))%></td>
			<td><%Response.Write(rs("repair.price"))%></td>
			<td><%Response.Write(rs("repair_date"))%></td>
			<td><%Response.Write(rs("repair.remark"))%></td>
			<td><%Response.Write(rs("staff_name"))%></td>
			<td><%Response.Write(rs("staff.contact"))%></td>
			<td><%Response.Write(rs("parts_name"))%></td>
			<td><%Response.Write(rs("number"))%></td>
		</tr>
	</table>
</div>
<%rs.close%>
<!--#include file="./layout/staffFootLayout.asp"-->