<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">查询今年各个月份的账单</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">请选择</span>
					<select name="month">
						<%
							curDate=date()
							curMonth=month(curDate)
							for i=0 to curMonth-1
								month_i=curMonth-i
						%>
						<option><%=month_i%></option>
						<%next%>
					</select>
					<span class="add-on">月</span>
					<button class="btn btn-mini btn-info" formaction="./queryMBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->