<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span6 offset1 well">
	<form method="post">
		<p class="text-error">修改用户名</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">请输入您想要修改的用户名</span>
					<input class="input" name="newName" type="text">
					<button class="btn btn-mini btn-info" formaction="./checkUsrName.asp">检查此名称是否可用</button>
                </div>
			</tr>
		</table>
		<button class="btn btn-mini btn-success" formaction="./saveUsrName.asp">提交</button>
	</form>
</div>
<!--#include file="./layout/staffFootLayout.asp"-->