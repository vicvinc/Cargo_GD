<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	id=request("id")
	staffName=request.form("staffName")
	staffPhone=request.form("staffPhone")
	staffAddress=request.form("staffAddress")
	staffAge=request.form("staffAge")
	staffGender=request.form("staffGender")
	addToSys=request.form("addToSys")
	addDate=request.form("addDate")
	sql="select * from staff where usr_id="&id&""
	rs.open sql,conn,1,3
%>
<div class="well span6 offset1">
          <div class="hero-unit">
            <form method="post" action="./updateStaff.asp?id=<%=id%>">
              <fieldset>
                <p class="text-success">修改员工信息</p>
              <table class="table">
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>员工姓名</span>
                      <input name="staffName" type="text" placeholder="员工姓名" value="<%response.write(rs("staff_name"))%>">
                    </div>  
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>用户性别</span>
                      <select name="staffGender" placeholder="性别">
                      	<option><%response.write(rs("gender"))%></option>
                      	<%
                      		if rs("gender")="男" then

                      	%>
                        	<option>女</option>
                        <%else%>
                        	<option>男</option>
                        <%end if%>
                      </select>
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>联系电话</span>
                      <input class="input" name="staffPhone" type="text" placeholder="联系电话" value="<%response.write(rs("contact"))%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">联系地址</span>
                      <input class="input" name="staffAddress" type="text" placeholder="联系地址" value="<%response.write(rs("address"))%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">员工年龄</span>
                      <input class="input" name="staffAge" type="text" placeholder="年龄" value="<%response.write(rs("age"))%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">添加日期</span>
                      <input class="input" name="addDate" type="text" placeholder="添加日期" value="<%response.write(rs("addDate"))%>">
                    </div>
                </tr>
                <%
                	set rs1=server.CreateObject("adodb.recordset")
			  			sql1="select * from staffUsr where usr_id="&id&""
			  			rs1.open sql1,conn,1,3
			  			if rs1.eof Then
			  			%>
			  	<tr>
                  <input type="checkbox" name="check" value="addToSys">
                  <p class="btn btn-info">将此员工不是系统用户，是否添加至系统用户</p>
                </tr>
                <%
			  			else 
			  				%>
			  	<tr>
                  <input type="checkbox" name="check" value="deleteFromSys">
                  <p class="btn btn-info">将此员工是系统用户，是否从系统用户中删除</p>
               	</tr>	
				<%
			  			end if
			  			rs1.close 
                %>
              </table>
	          <button type="submit" class="btn btn-success" value="reset">提交</button>
	          <button type="reset" class="btn btn-inverse" value="reset">重置</button>
                      
              </fieldset>
            </form>
</div><!--/span-->

<!--#include file="./layout/adminFootLayout.asp"-->