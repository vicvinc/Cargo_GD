<!DOCtype html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>help</title>
    <!-- Le styles -->
    <link href="./html/css/main.css" rel="stylesheet">
    <script type="text/javascript" src="./html/js/jquery.js"></script>
    <script type="text/javascript" src="./html/js/side.js"></script>
    <style>
      body{
        padding-top:80px;
      }
    </style>
</head>
<body>
	<!--nav bar-->
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
          <div class="container">
              <a class="brand" href="index.asp">
                <i class="icon-home"></i>主页
              </a>
              <div class="nav-collapse collapse">
                  <ul class="nav">
                      <li class="cative">
                        <a class="cative">
                        </a> 
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
	<!--container-->
	<div calss="container">
		<div class="row-fluid">
			<hr>
			<div class="span3">
				<div class="well" style="padding: 8px 0;">
	        <ul class="nav nav-list affix"">
	          <li class="divider"></li>
	          <li class="nav-header">常见问题</li>
	          <li class="active"><a href="#SysQuery">系统查询</a></li>
	          <li><a href="#balance">账单问题</a></li>
	          <li><a href="#parts">配件管理</a></li>
	          <li class="divider"></li>
	          <li class="nav-header">系统帮助</li>
	          <li><a href="#login">登录相关</a></li>
	          <li><a href="#database">数据库相关</a></li>
	        </ul>
	      </div>
			</div>
			<div class="span7">
				<div class="wall pull-right">
					<table class="table table-hover">
						<thead>
							<th>项目</th>
							<th>相关说明</th>
						</thead>
						<tbody>
							<tr>
								<td><p class="text-success" id="SysQuery">系统查询<p></td>
								<td><p class="text-info">员工查询系统包括：用户信息查询、账单信息查询、配件信息查询
									管理员查询系统包括：员工信息查询、营业额查询、配件入库售出查询
									其中，用户信息查询不能为空，账单查询中顾客信息必须和车辆维修信息匹配，配件查询中配件信息和供应商信息必须匹配</p>
								</td>
							</tr>
							<tr>
								<td id="balance">账单问题</td>
								<td><p class="text-info">账单是由车辆维修产生，记录了车辆的维修还有配件使用，人工维修费用等信息，在账单查询中可以查看相应的发票信息，并且可以打印发票</p>
								</td>
							</tr>
							<tr>
								<td id="parts">配件管理</td>
								<td><p class="text-info">员工配件管理：通过新建配件信息建立库存中没有的配件信息，配件查询中可以查询配件信息，也可在此修改和和删除配件信息
									管理员配件管理：通过查询来查看配件的入库信息和售出信息，也可以查看配件的信息，但是不能修改和删除配件的信息，因为修改配件信息不属于管理员的工作</p>
								</td>
							</tr>
							<tr>
								<td id="login">登录相关</td>
								<td><p class="text-info">登录必须选择使用者的身份，超级管理员和普通管理员都用管理员的身份登录，身份保持时间为10分钟，超出10分钟系统自动登出，需要重新登录。员工可以在自己工作的界面管理自己的个人信息，如修改账号名、密码等等。管理员可以修改自己的账户名和密码。</p>
								</td>
							</tr>
							<tr>
								<td id="database">数据库相关</td>
								<td><p class="text-info">本系统数据库采用MS Access数据库存储底层数据，有关操作过程中出现的数据无无法访问或者访问出错等信息请联系管理员进行处理</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class ="page-header">
    <p class="pull-right"><a href="#">回到顶端</a></p>
  </div>
  <footer class="footer">
    <div class="container">
      <p>Made By M@twitter</a></p>
      <p>开源依照 <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a> 发布。 文档依照 <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a> 发布。</p>
      <p>Icons取自 <a href="http://glyphicons.com">Glyphicons Free</a>，授权依照 <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>。</p>
    </div>
  </footer>
</body>
</html>
