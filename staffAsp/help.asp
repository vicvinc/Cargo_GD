<!DOCtype html>
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="utf-8">
    <title>help</title>
    <!-- Le styles -->
    <link rel="stylesheet" type="text/css" href="./metro-bootstrap/css/metro-bootstrap.css">
    <script type="text/javascript" src="js/bootstrap-alert.js"></script>
    <style>
      body{
        padding-top:80px;
      }
    </style>
</head>
<body>
	<!--nav bar-->
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
          <div class="container">
              <a class="brand" href="index.asp">
                <i class="icon-home"></i>主页
              </a>
              <div class="nav-collapse collapse">
                  <ul class="nav">
                      <li class="cative">
                        <a class="cative">
                          <i class="icon-question-sign"></i>帮助
                        </a> 
                      </li>
                  </ul>
              </div>
          </div>
      </div>
  </div>
	<!--container-->
	<div calss="container">
		<div class="row-fluid">
			<hr>
			<div class="span3">
				<div class="well" style="padding: 8px 0;">
	        <ul class="nav nav-list">
	          <li class="nav-header">常见问题</li>
	          <li class="active"><a href="#">问题1</a></li>
	          <li><a href="#">问题1</a></li>
	          <li><a href="#">问题1</a></li>
	          <li class="nav-header">系统帮助</li>
	          <li><a href="#">问题1</a></li>
	          <li><a href="#">问题1</a></li>
	          <li class="divider"></li>
	          <li><a href="#">更多</a></li>
	        </ul>
	      </div>
			</div>
			<div class="span7">
				<div class="wall pull-right">
					<table class="table table-hover">
						<thead>
							<th>项目</th>
							<th>相关说明</th>
						</thead>
						<tbody>
							<tr>
								<td><p class="text-success">项目<p></td>
								<td><p class="text-info">sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor</p>
								</td>
							</tr>
							<tr>
								<td>帮助项目ose</td>
								<td><p class="text-info">sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor</p>
								</td>
							</tr>
							<tr>
								<td>帮助项目ose</td>
								<td><p class="text-info">sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor</p>
								</td>
							</tr>
							<tr>
								<td>帮助项目ose</td>
								<td><p class="text-info">sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor</p>
								</td>
							</tr>
							<tr>
								<td>帮助项目ose</td>
								<td><p class="text-info">sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor id nibh ultricies vehicula ut id elit. Morbi leo risus, porta ac consectetur ac, vestibulum at eros. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.Donec sed odio dui. Etiam porta sem malesuada magna mollis euismod. Nullam id dolor</p>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class ="page-header">
    <p class="pull-right"><a href="#">回到顶端</a></p>
  </div>
  <footer class="footer">
    <div class="container">
      <p>Made By M@twitter</a></p>
      <p>开源依照 <a href="http://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License v2.0</a> 发布。 文档依照 <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a> 发布。</p>
      <p>Icons取自 <a href="http://glyphicons.com">Glyphicons Free</a>，授权依照 <a href="http://creativecommons.org/licenses/by/3.0/">CC BY 3.0</a>。</p>
    </div>
  </footer>
</body>
</html>
