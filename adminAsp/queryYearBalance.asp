<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">查询年度账单信息</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">请选择年度</span>

					<select name="year">
						<%
							curDate=date()
							curYear=year(curDate)
							for i=0 to 10 
								year_i=curYear-i 
						%>
						<option><%=year_i%></option>
						<%next%>
					</select>
					<button class="btn btn-mini btn-info" formaction="./queryYBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->