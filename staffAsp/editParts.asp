<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	id=request("id")
	sql="select * from parts,supplier where parts.parts_id="&id
	rs.open sql,conn,1,1
%>
<div class="span8 offset1 well">
	<p class="text-info">修改配件信息</p>
	<form method="post" action="./updateParts.asp?id=<%=id%>">
        <table class="table">
          <tr>
            <td>
              <table class="table">
                <tr>
                  <h4>新配件信息</h4>
                </tr>
                <tr class="">
                    <div class="input-prepend"> 
                      <span class="add-on"><font color="red">*</font>配件名称</span>
                      <input name="partsName" type="text" placeholder="配件名称" value="<%=rs("parts_name")%>">
                    </div>  
                </tr>
                <tr class="">
                	<div class="controls">
		              <div class="input-append">
                      	<span class="add-on"><font color="red">*</font>配件价格</span>
		                <input name="partsPrice" type="text" placeholder="配件价格" value="<%=rs("price")%>">
		                <span class="add-on">.00￥</span>
		              </div>
		            </div>
                </tr>
              </table>
            </td>
            <td>
              <table class="table table-hover">
                <tr>
                  <h4>供应商信息</h4>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>供应商名</span>
                      <input class="input" name="supplierName" type="text" placeholder="供应商" value="<%=rs("supplier_name")%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>联系电话</span>
                      <input class="input" name="contactPhone" type="text" placeholder="联系电话" value="<%=rs("contact")%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">联系地址</span>
                      <input class="input" name="contactAddr" type="text" placeholder="联系地址" value="<%=rs("address")%>">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">公司名称</span>
                      <input class="input" name="companyName" type="text" placeholder="公司" value="<%=rs("company")%>">
                    </div>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <div>
          <p>配件备注信息:</p>
          <textarea class="span7" name="partsRemark"></textarea>
        </div>
        <input type="reset" class="btn-inverse pull-right" value="reset">
        <input type="submit" class="btn-success pull-right" value="submit">
    </form>
</div>
<%rs.close%>
<!--#include file="./layout/staffFootLayout.asp"-->