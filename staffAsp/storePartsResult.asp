<!--#include file="./layout/staffMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<%
	sql=session("storePartsSql")
	rs.open sql,conn,1,3
%>
<div class="span9 well">
	<p class="text-success">存单信息</p>
	<table class="table">
	    <th>配件编号</th>
	    <th>配件ID</th>
	    <th>配件名</th>
	    <th>配件价格</th>
	    <th>配件备注信息</th>
	    <tr>
	    	<td><%Response.Write(rs("parts.ID"))%></td>
			<td><%Response.Write(rs("parts.parts_id"))%></td>
			<td><%Response.Write(rs("parts_name"))%></td>
			<td><%Response.Write(rs("parts.price"))%></td>
			<td><%Response.Write(rs("remark"))%></td>	
	    </tr>
		<th>供应商编号</th>
	    <th>供应商</th>
	    <th>联系电话</th>
	    <th>联系地址</th>
	    <th>资料建立日期</th>
			<tr>
				<td><%Response.Write(rs("supplier.ID"))%></td>
				<td><%Response.Write(rs("supplier_name"))%></td>
				<td><%Response.Write(rs("contact"))%></td>
				<td><%Response.Write(rs("address"))%></td>
				<td><%Response.Write(rs("supplyDate"))%></td>
			</tr>
      	<th>存入编号</th>
	    <th>存入日期</th>
	    <th>存入数量</th>
	    <th>库存数量</th>
	    <th>公司</th>
		    <tr>
				<td><%Response.Write(rs("store.ID"))%></td>
				<td><%Response.Write(rs("storeDate"))%></td>
				<td><%Response.Write(rs("store.number"))%></td>
				<td><%Response.Write(rs("storage.number"))%></td>
				<td><%Response.Write(rs("company"))%></td>
			</tr>
		<%rs.close%>
	</table>

<!--#include file="./layout/staffFootLayout.asp"-->