<!--#include file="./layout/adminMainLayout.asp"-->
<div class="span9">
          <div class="hero-unit">
            <h3>欢迎来到管理员工作界面！</h3>
            <p>在这里您可以通过左侧的标题栏来管理您的日常工作</p>
          </div>
          <div class="row-fluid">
            <div class="span4">
              <h3>员工管理</h3>
              <p>这里用来添加和管理员工的日常信息</p>
            </div><!--/span-->
            <div class="span4">
              <h3>营业查询</h3>
              <p>这里查询维修店的营业情况，可以按照周、月、季来查询营业情况</p>
            </div><!--/span-->
            <div class="span4">
              <h3>配件查询</h3>
              <p>这里可以查询配件的存入和售出情况</p>
            </div><!--/span-->
          </div><!--/row-->
        </div><!--/span-->
      </div><!--/row-->

<!--#include file="./layout/adminFootLayout.asp"-->