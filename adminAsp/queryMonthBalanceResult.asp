<!--#include file="./layout/adminMainLayout.asp"-->
<!--#include file="./../dbConn.asp"-->
<div class="span8 well">
	<form method="post">
		<p class="text-error">查询今年各个月份的账单</p>
		<table class="table table-hover">
			<tr>
				<div class="input-prepend">
					<span class="add-on">请选择</span>
					<select name="month">
						<%
							curDate=date()
							curMonth=month(curDate)
							for i=0 to curMonth-1
								month_i=curMonth-i
						%>
						<option><%=month_i%></option>
						<%next%>
					</select>
					<span class="add-on">月</span>
					<button class="btn btn-mini btn-info" formaction="./queryMBalanceCheck.asp">查询</button>
                </div>
			</tr>
		</table>
	</form>
<%	
	queryMonths=session("Month")
	curDate=date()
	curMonth=month(curDate)
	curYear=year(curDate)
	startDate=DateSerial(curYear,queryMonths,1)
	endDate=DateSerial(curYear,queryMonths+1,1)
	sumBalance=0
%>
<p>查询<%=queryMonths%>月的账单</p>
<p class="text-info"><%=startDate%>至<%=endDate%>的营业账单</p>
<%
	sql="select * from balance,repair,staff where balance_date between #"&startDate&"# and #"&endDate&"# and balance.balance_id=repair.balance_id and staff.usr_id=repair.usr_id"
	response.write"'"&sql&"'"
	rs.open sql,conn,1,3
	if rs.eof then
        response.write ("<script>alert('没有指定日期内的账单!');</script>")
    else
		do until rs.eof
		    balanceAmount=rs("balance.price")
			sumBalance=sumBalance+balanceAmount
		rs.movenext
		loop
		rs.close
		sql="select * from balance,repair,staff where balance_date between #"&startDate&" # and #"&endDate&"# and balance.balance_id=repair.balance_id and staff.usr_id=repair.usr_id"
		rs.open sql,conn,1,3
        rs.PageSize=1 '//'pagesize属性指定了每页要显示的记录条数 
        Page = CLng(Request("Page")) 'string型转化为long型 
        If Page < 1 Then Page = 1 
        If Page > rs.PageCount Then Page = rs.PageCount 
        If Page <> 1 Then 
            Response.write"<a class='btn btn-info' href=./queryMonthBalanceResult.asp?Page=1>1</a>" 
            Response.Write"<a class='btn btn-success' href=./queryMonthBalanceResult.asp?Page="&(Page-1)&">pre</a>" 
        End If 
        If Page <> rs.PageCount Then 
            Response.Write "<a class='btn btn-success' href=./queryMonthBalanceResult.asp?Page="&(Page+1)&">next</A>" 
            Response.Write "<a class='btn btn-info' href=./queryMonthBalanceResult.asp?Page="&rs.PageCount&">last</A>" 
        End If 
        Response.write"<p class='btn btn-inverse'>"&Page&"/"&rs.PageCount&"</p>"
        rs.AbsolutePage = Page '//'把页码赋给absolutepage属性从而知当前页的首条记录号 
        'For iPage = 1 To rs.PageSize
%>
          <table class="table table-striped table-bordered table-condensed">
            <%For iPage = 1 To rs.PageSize
                if rs.eof then
                  Exit For
                else %>
            <th>账单编号</th>
			<th>账单日期</th>
			<th>账单状态</th>
			<th>账单费用</th>
			<th>维修编号</th>
			<th>维修费用</th>
			<th>维修日期</th>
			<th>维修状态</th>
			<th>维修人员</th>
			<th>维修人员联系方式</th>
			<th>配件及使用情况</th>
			<th>配件数量</th>
			<tr>
				<td><%Response.Write(rs("balance.balance_id"))%></td>
				<td><%Response.Write(rs("balance_date"))%></td>
				<td><%Response.Write(rs("balance.remark"))%></td>
				<td>
					<%
						Response.Write(rs("balance.price"))
					%>
				</td>
				<td><%Response.Write(rs("repair.repair_id"))%></td>
				<td><%Response.Write(rs("repair.price"))%></td>
				<td><%Response.Write(rs("repair_date"))%></td>
				<td><%Response.Write(rs("repair.remark"))%></td>
				<td><%Response.Write(rs("staff_name"))%></td>
				<td><%Response.Write(rs("contact"))%></td>
				<td colspan="2">
                    <table class="table">
                        <th>配件</th>
                        <th>配件数量</th>
                         <%		
                     		repairID=rs("repair.repair_id")
							set rs1=server.CreateObject("adodb.recordset")
							sql1="select * from use,parts where use.parts_id=parts.parts_id and use.repair_id="&repairID&""
							'response.write"'"&sql1&"'"
							rs1.open sql1,conn,1,3
							do until rs1.eof
                           %>
                      <tr>
                        <td><%Response.Write(rs1("parts_name"))%></td>
                        <td><%Response.Write(rs1("number"))%></td>
                      </tr>
                    <% 
                        rs1.MoveNext
                        loop
                        rs1.close
                    %>
                    </table>
                </td> 
<%
          
                	rs.MoveNext
              	end if
            Next
    end if
%>
</table>
<h3 class="text-error">阶段总营业额：<%=sumBalance%>￥</h3>
</div>
<!--#include file="./layout/adminFootLayout.asp"-->