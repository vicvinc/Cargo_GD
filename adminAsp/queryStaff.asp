<!--#include file="./layout/adminMainLayout.asp"-->
<div class="span9 well">
	        <p class="text-success">选择要查询的员工</p>
	        <p>（若查询信息为空，默认查询所有员工信息）</p>
	        <form class="form-inline" method="post" action="./queryStaffCheck.asp">
	            <span>员工姓名</span>
	            <input class="input-small" name="staffName" type="text" placeholder="员工名">
	            <span>创建日期</span>
	            <input class="input-small" name="addDate" type="text" placeholder="创建日期">
	            <span>性别</span>
	            <select class="input-small" name="staffGender">
	            	<option>男</option>
	            	<option>女</option>
	            </select>
	            <span>年龄</span>
	            <input class="input-small" name="staffAge" type="text" placeholder="年龄">
	            <button type="submit" class="btn btn-small btn-info">查询</button>
	         </form>
        </div>
<!--#include file="./layout/adminFootLayout.asp"-->