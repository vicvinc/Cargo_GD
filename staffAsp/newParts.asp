<!--#include file="./layout/staffMainLayout.asp"-->
<div class="span7 offset1">
    <form  class="" method="post" action="./saveParts.asp">
      <fieldset>
        <p class="text-success">建立新配件信息</p>
        <table class="table">
          <tr>
            <td>
              <table class="table">
                <tr>
                  <h4>新配件信息</h4>
                </tr>
                <tr class="">
                    <div class="input-prepend"> 
                      <span class="add-on"><font color="red">*</font>配件名称</span>
                      <input name="partsName" type="text" placeholder="配件名称">
                    </div>  
                </tr>
                <tr class="">
                    <div class="input-prepend"> 
                      <span class="add-on">配件型号</span>
                      <input name="partsType" type="text" placeholder="配件型号">
                    </div>  
                </tr>
                <tr class="">
                    <div class="input-prepend"> 
                      <span class="add-on">配件材质</span>
                      <input name="partsMeterial" type="text" placeholder="配件材质">
                    </div>  
                </tr>
                <tr class="">
                	<div class="controls">
  		              <div class="input-append">
                        	<span class="add-on"><font color="red">*</font>配件价格</span>
  		                <input name="partsPrice" type="text" placeholder="配件价格">
  		                <span class="add-on">.00￥</span>
  		              </div>
  		            </div>
                </tr>
              </table>
            </td>
            <td>
              <table class="table table-hover">
                <tr>
                  <h4>供应商信息</h4>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>供应商名</span>
                      <input class="input" name="supplierName" type="text" placeholder="供应商">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>联系电话</span>
                      <input class="input" name="contactPhone" type="text" placeholder="联系电话">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on">联系地址</span>
                      <input class="input" name="contactAddr" type="text" placeholder="联系地址">
                    </div>
                </tr>
                <tr class="">
                    <div class="input-prepend">
                      <span class="add-on"><font color="red">*</font>公司名称</span>
                      <input class="input" name="companyName" type="text" placeholder="公司">
                    </div>
                </tr>
              </table>
            </td>
          </tr>
        </table>
        <div>
          <p>配件备注信息:</p>
          <textarea class="span7" name="partsRemark"></textarea>
        </div>
        <input type="submit" class="btn-success" value="提交">
        <input type="reset" class="btn-inverse" value="重置">
      </fieldset>
    </form>
</div><!--/span-->
<!--#include file="./layout/staffFootLayout.asp"-->