<!--#include file="./layout/adminMainLayout.asp"-->
<div class="well span6 offset1">
          <div class="hero-unit">
            <form method="post" action="./saveStaff.asp">
              <fieldset>
                <p class="text-success">建立新员工信息</p>
                      <table class="table">
                        <tr>
                          <h4>新员工信息</h4>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>员工姓名</span>
                              <input name="staffName" type="text" placeholder="员工姓名">
                            </div>  
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>用户性别</span>
                              <select name="staffGender" placeholder="性别">
                                <option>男</option>
                                <option>女</option>
                              </select>
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on"><font color="red">*</font>联系电话</span>
                              <input class="input" name="staffPhone" type="text" placeholder="联系电话">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">联系地址</span>
                              <input class="input" name="staffAddress" type="text" placeholder="联系地址">
                            </div>
                        </tr>
                        <tr class="">
                            <div class="input-prepend">
                              <span class="add-on">员工年龄</span>
                              <input class="input" name="staffAge" type="text" placeholder="联系地址">
                            </div>
                        </tr>
                        <tr>
                          <input type="checkbox" name="addToSys" value="addToSys">
                          <p class="btn btn-info">将此员工添加至系统用户</p>
                        </tr> 
                      </table>
                      <button type="submit" class="btn btn-success" value="reset">提交</button>
                      <button type="reset" class="btn btn-inverse" value="reset">重置</button>
                      
              </fieldset>
            </form>
        </div><!--/span-->
<!--#include file="./layout/adminFootLayout.asp"-->